#!/usr/bin/env python2.7
import numpy
#import playtone
import pyaudio
from notes import *
import sys
import tones

p = pyaudio.PyAudio()
stream = p.open(
    format=pyaudio.paFloat32,
    channels=1,
    rate=44100,
    output=True,
)


def l(x): # make a note twice as long
    return (x[0], x[1]*2)
def s(x): # make a note half as long
    return (x[0], x[1]*0.5)

INDEX = 0
TRACK_LENGTH = 0
ALL = numpy.linspace(0, 1, 44100)
TRACK_2 = numpy.linspace(0, 1, 44100)
GLOBAL_SPEEDUP = 0.1

def tone(note):
    global INDEX
    global ALL
    global TRACK_LENGTH
    shortened_note = (note[0], note[1]*GLOBAL_SPEEDUP)
    #arr = tones.xylophone(shortened_note)
    arr = tones.pleasant(shortened_note)

    TRACK_LENGTH += note[1]

    # append to global array
    ALL = numpy.concatenate((ALL, arr))
    INDEX = INDEX + 1


def track_2():
    """
    here's the drum track ;D sweet ass drums, mmMMMhh, that's some good shit right there
    this function must be run after tone() above, kek also index must be reset kek enjoy
    have a tone that's not the same length? shits gonna be offbeat1!1! :DDD etc etc
    """
    global TRACK_2
    global INDEX
    kick = tones.basekick(1 * GLOBAL_SPEEDUP)
    hardkick = tones.hardkick(1 * GLOBAL_SPEEDUP)
    hihat = tones.hihat(1 * GLOBAL_SPEEDUP)
    for kek in range(TRACK_LENGTH):
        shittycomposite = tones.pause(1 * GLOBAL_SPEEDUP)
        if INDEX % 2 == 0:
            shittycomposite = numpy.add(shittycomposite, hihat)*.2 # subdue the hihat a bit
        if INDEX % 4 == 0:
            shittycomposite = numpy.add(shittycomposite, kick)
        if INDEX % 8 == 0:
            shittycomposite = numpy.add(shittycomposite, hardkick) # spice it up!
        TRACK_2 = numpy.concatenate((TRACK_2, shittycomposite))
        INDEX = INDEX + 1



# C4 is bottom one, C5 is top one in soprano key

INTRO_STAFF1 = [
    DS5, E5, FS5,
    s(PAUSE),
    B5, E5, DS5, E5, FS5, B5, CS6, B5, AS5, B5,
    s(PAUSE)
]

INTRO_STAFF2 = [
    FS5,
    s(PAUSE),
    DS5, E5, FS5,
    s(PAUSE),
    l(B5), CS6, AS5, B5, CS6,
    E6, DS6, E6, CS6
]

MAIN_STAFF1 = [
    l(FS5), l(GS5),
    B4, l(B4),
    CS5, D5, CS5, l(B4),
    l(B4), l(B4)
]

MAIN_STAFF2 = [
    l(D5), D5, CS5,
    B4, CS5, DS5, FS5,
    GS5, DS5, FS5, CS5,
    DS5, B4, CS5, B4
]

MAIN_STAFF3 = [
    l(DS5), l(FS5),
    GS5, DS5, FS5, CS5,
    DS5, B4, D5, DS5,
    D5, CS5, B4, CS5
]

MAIN_STAFF4 = [
    l(D5), B4, CS5,
    DS5, FS5, CS5, D5,
    CS5, B4, l(CS5),
    l(B4), l(CS5)
]
MAIN_STAFF5 = [
    l(FS5), l(GS5),
    D5, l(DS5), CS5,
    D5, CS5, l(B4),
    l(B4), l(CS5)
]
MAIN_STAFF6 = [
    l(D5), D5, CS5,
    B4, CS5, DS5, FS5,
    GS5, DS5, FS5, CS5,
    DS5, B4, CS5, B4
]
MAIN_STAFF7 = [
    l(DS5), l(FS5),
    GS5, DS5, FS5, CS5,
    DS5, B4, D5, DS5,
    D5, CS5, B4, CS5
]
MAIN_STAFF8 = [
    l(D5), B4, CS5,
    DS5, FS5, CS5, D5,
    CS5, B4, l(CS5),
    l(B4), l(B4)
]

STAFF_9 = [
    l(B4), FS4, GS4, l(B4),
    FS4, GS4, B4, CS5, DS5, B4, E5, DS5, E5, FS5, l(B4),
]

STAFF_10 = [
    l(B4), FS4, GS4, B4, FS4,
    E5, DS5, CS5, B4,
    FS4, DS4, E4, FS4,
]

STAFF_11 = [
    l(B4), FS4, GS4, B4, FS4,
    E5, DS5, E5, FS5,
    l(B4), l(AS4)
]

STAFF_12 = [
    l(B4), FS4, GS4, B4, FS4,
    E5, DS5, E5, FS5,
    l(B4), l(CS5)
]

# [tone(x) for x in INTRO_STAFF1]
# [tone(x) for x in INTRO_STAFF2]
# tone(PAUSE)

[tone(x) for x in MAIN_STAFF1]
[tone(x) for x in MAIN_STAFF2]
[tone(x) for x in MAIN_STAFF3]
[tone(x) for x in MAIN_STAFF4]

[tone(x) for x in MAIN_STAFF5]
[tone(x) for x in MAIN_STAFF6]
[tone(x) for x in MAIN_STAFF7]
[tone(x) for x in MAIN_STAFF8]

[tone(x) for x in STAFF_9]
[tone(x) for x in STAFF_10]
[tone(x) for x in STAFF_9]
[tone(x) for x in STAFF_11]

[tone(x) for x in STAFF_9]
[tone(x) for x in STAFF_10]
[tone(x) for x in STAFF_9]
[tone(x) for x in STAFF_12]

# [tone(x) for x in [C0, C1, C2, C3, C4, C5, C6, C7, C8, C9, C10, CS0, CS1, CS2, CS3, CS4, CS5, CS6, CS7, CS8, CS9, CS10, D0, D1, D2, D3, D4, D5, D6, D7, D8, D9, D10, DS0, DS1, DS2, DS3, DS4, DS5, DS6, DS7, DS8, DS9, DS10, E0, E1, E2, E3, E4, E5, E6, E7, E8, E9, E10, F0, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, FS0, FS1, FS2, FS3, FS4, FS5, FS6, FS7, FS8, FS9, FS10, G0, G1, G2, G3, G4, G5, G6, G7, G8, G9, G10, GS0, GS1, GS2, GS3, GS4, GS5, GS6, GS7, GS8, GS9, GS10, A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, AS0, AS1, AS2, AS3, AS4, AS5, AS6, AS7, AS8, AS9, AS10, B0, B1, B2, B3, B4, B5, B6, B7, B8, B9, B10]]

# [tone(x) for x in [C0, CS0, D0, DS0, E0, F0, FS0, G0, GS0, A0, AS0, B0, C1, CS1, D1, DS1, E1, F1, FS1, G1, GS1, A1, AS1, B1, C2, CS2, D2, DS2, E2, F2, FS2, G2, GS2, A2, AS2, B2, C3, CS3, D3, DS3, E3, F3, FS3, G3, GS3, A3, AS3, B3, C4, CS4, D4, DS4, E4, F4, FS4, G4, GS4, A4, AS4, B4, C5, CS5, D5, DS5, E5, F5, FS5, G5, GS5, A5, AS5, B5, C6, CS6, D6, DS6, E6, F6, FS6, G6, GS6, A6, AS6, B6, C7, CS7, D7, DS7, E7, F7, FS7, G7, GS7, A7, AS7, B7, C8, CS8, D8, DS8, E8, F8, FS8, G8, GS8, A8, AS8, B8, C9, CS9, D9, DS9, E9, F9, FS9, G9, GS9, A9, AS9, B9, C10, CS10, D10, DS10, E10, F10, FS10, G10, GS10, A10, AS10, B10]]

INDEX = 0
track_2()
ALL = numpy.add(ALL, TRACK_2)

print("done synthesizing, playing...")
#playtone.play_buffer(tones.ALL.astype("float32"))

stream.start_stream()
stream.write(ALL.astype("float32"), num_frames=len(ALL))


# stop stream (4)
stream.stop_stream()
stream.close()

# close PyAudio (5)
p.terminate()
