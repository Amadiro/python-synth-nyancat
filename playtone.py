#!/usr/bin/env python

# Simple test script that plays (some) wav files

from __future__ import print_function

import sys
import wave
import getopt
import alsaaudio

device = alsaaudio.PCM()

def play_buffer(array):
    device.setchannels(1)
    device.setrate(44100)
    device.setformat(alsaaudio.PCM_FORMAT_FLOAT_LE)
    device.setperiodsize(320)
    device.write(array)
    # Otherwise we assume signed data, little endian
    #elif f.getsampwidth() == 2:
    #    device.setformat(alsaaudio.PCM_FORMAT_S16_LE)
    #elif f.getsampwidth() == 3:
    #    device.setformat(alsaaudio.PCM_FORMAT_S24_LE)
    #elif f.getsampwidth() == 4:
    #    device.setformat(alsaaudio.PCM_FORMAT_S32_LE)
    
def tone(freq, length):
    arr = numpy.sin(numpy.linspace(0, 2*numpy.pi*freq*length, 44100*length))
    playtone.play_buffer(arr)
