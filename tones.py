import numpy

def pleasant(note):
    freq, length = note
    # replace with sin to get true frequencies
    xs = numpy.linspace(0, 2*numpy.pi*freq*length, 44100*length)
    attenuation = numpy.linspace(1, 0, 44100*length)*numpy.linspace(0, 1, 44100*length)
    arr = numpy.cos(xs)*attenuation
    return arr

def grating(note):
    # b gratful
    freq, length = note
    length = length*0.1
    # replace with sin to get true frequencies
    xs = numpy.linspace(0, 2*numpy.pi*freq*length, 44100*length)
    attenuation = numpy.linspace(1, 0, 44100*length)*numpy.linspace(0, 1, 44100*length)
    arr = numpy.tan(xs)*attenuation

    #playtone.play_buffer(arr.astype("float32"))
    global ALL
    ALL = numpy.concatenate((ALL, numpy.zeros(shape=(100))))
    ALL = numpy.concatenate((ALL, arr))


def basekick(length):
    attenuation = numpy.linspace(1, 0, 44100*length)
    wave = numpy.sin(numpy.linspace(0, 2*numpy.pi*60*length, 44100*length))*attenuation*attenuation*attenuation
    return wave

def hardkick(length):
    attenuation = numpy.linspace(1, 0, 44100*length)
    xs = numpy.linspace(0, 2*numpy.pi*length, 44100*length)
    noise = numpy.random.rand(len(xs))*attenuation*attenuation*attenuation*attenuation*attenuation*attenuation
    return noise

def pause(length):
    silence = numpy.linspace(0, 0, 44100*length)
    return silence

def hihat(length):
    attenuation = numpy.linspace(1, 0, 44100*length)
    xs = numpy.linspace(0, 2*numpy.pi*length, 44100*length)
    noise = numpy.random.rand(len(xs))*attenuation*attenuation*attenuation*attenuation*attenuation
    return noise

def xylophone(note):
    freq, length = note
    xs = numpy.linspace(0, 2*numpy.pi*freq*length, 44100*length)
    attenuation = numpy.linspace(1, 0, 44100*length)
    arr = numpy.cos(xs)*attenuation

    #playtone.play_buffer(arr.astype("float32"))
    return arr # numpy.concatenate((arr, numpy.zeros(shape=(100))))

def backwards(note):
    freq, length = note
    length = length*0.1
    # replace with sin to get true frequencies
    xs = numpy.linspace(0, 2*numpy.pi*freq*length, 44100*length)
    attenuation = numpy.linspace(0, 1, 44100*length)
    arr = numpy.cos(xs)*attenuation

    #playtone.play_buffer(arr.astype("float32"))
    global ALL
    ALL = numpy.concatenate((ALL, numpy.zeros(shape=(100))))
    ALL = numpy.concatenate((ALL, arr))

def plain(note):
    freq, length = note
    length = length*0.1
    # replace with sin to get true frequencies
    xs = numpy.linspace(0, 2*numpy.pi*freq*length, 44100*length)
    arr = numpy.cos(xs)

    #playtone.play_buffer(arr.astype("float32"))
    global ALL
    ALL = numpy.concatenate((ALL, numpy.zeros(shape=(100))))
    ALL = numpy.concatenate((ALL, arr))

def pleasant2(note):
    freq, length = note
    length = length*0.1
    # replace with sin to get true frequencies
    xs = numpy.linspace(0, 2*numpy.pi*freq*length, 44100*length)
    attenuation = numpy.sin(numpy.linspace(0, numpy.pi, 44100*length))
    arr = numpy.cos(xs)*attenuation

    #playtone.play_buffer(arr.astype("float32"))
    global ALL
    ALL = numpy.concatenate((ALL, numpy.zeros(shape=(100))))
    ALL = numpy.concatenate((ALL, arr))

def slapstick(note):
    freq, length = note
    length = length*0.1
    # replace with sin to get true frequencies
    xs = numpy.linspace(0, 2*numpy.pi*freq*length, 44100*length)
    attenuation = 1.0 - numpy.sin(numpy.linspace(0, numpy.pi, 44100*length))
    arr = numpy.cos(xs)*attenuation

    #playtone.play_buffer(arr.astype("float32"))
    global ALL
    ALL = numpy.concatenate((ALL, numpy.zeros(shape=(100))))
    ALL = numpy.concatenate((ALL, arr))
